(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/script/signupBlock.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '9e7e3H0rdNKiJ7RLi+wBJOF', 'signupBlock', __filename);
// script/signupBlock.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var This;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        _this.alertBlock = null;
        _this.emterBtn = null;
        _this.backBtn = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    NewClass.prototype.onLoad = function () {
        this.initChildNode();
        This = this;
    };
    NewClass.prototype.start = function () {
        this.emterBtn.on(cc.Node.EventType.MOUSE_UP, this.onEnterPressed, this);
    };
    // update (dt) {
    // }
    NewClass.prototype.initChildNode = function () {
        this.userNameText = this.node.getChildByName("userName").getComponent(cc.EditBox);
        this.emailText = this.node.getChildByName("email").getComponent(cc.EditBox);
        this.passwordText = this.node.getChildByName("passward").getComponent(cc.EditBox);
        this.confirmPasswordText = this.node.getChildByName("confirmPassward").getComponent(cc.EditBox);
        this.emterBtn = cc.find("bottomBtns/emterBtn", this.node);
        this.backBtn = cc.find("bottomBtns/backBtn", this.node);
        this.alertBlock = cc.find("title/alertBlock", this.node);
    };
    NewClass.prototype.userPrompt = function (msg) {
        var prompt = "<color=#f2daa2>" + msg + "</c>";
        this.alertBlock.getChildByName("richtext").getComponent(cc.RichText).string = prompt;
        this.alertBlock.opacity = 255;
        this.scheduleOnce(function () {
            this.alertBlock.runAction(cc.fadeOut(0.8));
        }, 3.5);
    };
    NewClass.prototype.onEnterPressed = function (event) {
        // this.userPrompt("test test test test test test test test test test test test test test test test test test test test test test test test");
        // return;
        // cc.log(this.userNameText.string);
        // cc.log(this.emailText.string);
        // cc.log(this.passwordText.string);
        // cc.log(this.confirmPasswordText.string);
        if (this.userNameText.string == "") {
            this.userPrompt("please input the user name~");
            return;
        }
        if (this.emailText.string == "") {
            this.userPrompt("please input the email~");
            return;
        }
        if (this.passwordText.string == "") {
            this.userPrompt("please input the passward~");
            return;
        }
        if (this.confirmPasswordText.string == "") {
            this.userPrompt("please input the confirm passward~");
            return;
        }
        if (this.confirmPasswordText.string != this.passwordText.string) {
            this.userPrompt("confirm passward and passward are different!");
            return;
        }
        This.userPrompt("loading...");
        firebase.auth().createUserWithEmailAndPassword(this.emailText.string, this.passwordText.string).then(function (result) {
            This.userPrompt("login success!");
            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    var userID = user.uid;
                    var userNameRef = firebase.database().ref(userID + "/userName");
                    // var userLifeRef = firebase.database().ref(`${userID}/Life`);
                    // var userCoinRef = firebase.database().ref(`${userID}/Coin`);
                    // var userScoreRef = firebase.database().ref(`${userID}/Score`);
                    // cc.log(userNameVariable);
                    userNameRef.set(This.userNameText.string).then(function () {
                        This.userNameText.string = "";
                        This.emailText.string = "";
                        This.passwordText.string = "";
                        This.confirmPasswordText.string = "";
                    });
                    // userLifeRef.set(5);
                    // userCoinRef.set(0);
                    // userScoreRef.set(0);
                }
            });
        }).catch(function (error) {
            This.userNameText.string = "";
            This.emailText.string = "";
            This.passwordText.string = "";
            This.confirmPasswordText.string = "";
            var errorMessage = error.message;
            This.userPrompt(errorMessage);
            cc.log(errorMessage);
        });
    };
    __decorate([
        property(cc.Label)
    ], NewClass.prototype, "label", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=signupBlock.js.map
        